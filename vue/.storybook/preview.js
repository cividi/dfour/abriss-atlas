// import { withVuetify, withThemeProvider } from '@socheatsok78/storybook-addon-vuetify/dist/decorators'
import { withContexts } from '@storybook/addon-contexts/vue';
import { contexts } from "./contexts";
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import Vue from "vue";
import VueRouter from 'vue-router';
import VueI18n from "vue-i18n";
import Vuetify from 'vuetify/lib';
import vuetify from '../src/plugins/vuetify'
import { VApp } from  'vuetify/lib';

require('../src/assets/styles/main.css');

Vue.use(VueI18n)
Vue.use(VueRouter)
Vue.use(Vuetify)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      beforeEnter(to, from, next) {
        let lang = to.params.lang;
        if(!['de', 'fr', 'en', 'it'].includes(lang)) {
          lang = 'de';
        }
        if (i18n.locale !== lang) {
          i18n.locale = lang;
        }
        return next();
      },
      path: '/:lang',
      children: [
        {
          path: '',
          name: 'home'
        }
      ]
    }
  ]
});

const i18n = new VueI18n({
  locale: 'de',
  fallbackLocale: 'de',
  messages: {'de': {}, 'en': {}, 'fr': {}, 'it': {}}
});

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  viewport: {
    viewports: { 
      fullHD: {
        name: 'Full HD Screen',
        styles: {
          width: '1980px',
          height: '1080px',
        },
      },
      laptop: {
        name: 'Laptop',
        styles: {
          width: '1280px',
          height: '800px',
        },
      },
      ipadL: {
        name: 'iPad Landscape',
        styles: {
          width: '1024px',
          height: '768px',
        },
      },
      ...INITIAL_VIEWPORTS,
    },
  },
}

export const decorators = [
  // withVuetify,
  // withThemeProvider,
  withContexts(contexts),
  (story) => ({
    i18n,
    router,
    vuetify,
    components: { story, VApp },
    beforeCreate: function() {
      this.$root._i18n = this.$i18n;
    },
    template: '<v-app><story /></v-app>'
  }),
];