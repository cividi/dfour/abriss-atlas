const path = require('path');
const { VuetifyLoaderPlugin } = require('vuetify-loader')

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    '@storybook/addon-a11y',
    '@storybook/addon-contexts/register',
    '@socheatsok78/storybook-addon-vuetify',
  ],
  env: (config) => ({
    ...config,
    VUE_APP_DJANGOBASEURL: '',
    VUE_APP_GIT_VERSION: '1.0\t94e1c90',
    VUE_APP_PUBLIC_URL: 'http://localhost:8080'
  }),
  staticDirs: ['../public', { from: '../src/stories/assets', to: '/media/assets/' }],
  webpackFinal: async (config, { configType }) => {
    config.module.rules.push({
      resourceQuery: /blockType=i18n/,
      type: 'javascript/auto',
      loader: '@intlify/vue-i18n-loader'
    });
    config.module.rules.push({
      test: /\.s[ac]ss$/i,
      use: ["style-loader","css-loader", {
        loader: 'sass-loader',
        options: {
          prependData: '@import "./src/scss/variables.scss"'
        }
      }]
    });
    config.plugins.push(new VuetifyLoaderPlugin());
    config.resolve.alias = {
      ...config.resolve.alias,
      "@": path.resolve(__dirname, 'src/'),
      // react: "preact/compat",
      // "react-dom": "preact/compat",
    };

    return config;
  },
  "framework": "@storybook/vue"
}