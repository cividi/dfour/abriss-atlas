const createContext = (initialValue) => {
    return {
      name: `Context.i18n`,
      props: ["value"],
      watch: {
        value: function(newValue, oldValue) {
          this.$root._i18n.locale = newValue.locale;
        }
      },
      template: `<div><slot /></div>`
    };
  };
  
  const i18nContext = createContext({
    locale: "de"
  });
  
  export const contexts = [
    {
      icon: "globe",
      title: "locale",
      components: [i18nContext],
      params: [
        {
            name: "Deutsch",
            props: {
                value: { locale: "de" }
            }
        },
        {
          name: "English",
          props: {
            value: { locale: "en" }
          }
        },
        {
          name: "Français",
          props: {
            value: { locale: "fr" }
          }
        },
        {
            name: "Italiano",
            props: {
              value: { locale: "it" }
            }
          }
      ]
    }
  ];