import test from 'ava';
import { shallowMount } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import store from '../../src/store';
import i18n from '../../src/trans';
import HeroSection from '../../src/components/landing/HeroSection.vue';

const router = new VueRouter();
const vuetify = new Vuetify();

test('landing/HeroSection.vue', async (t) => {
  const wrapper = shallowMount(HeroSection, {
    vuetify,
    i18n,
    store,
    router
  });

  t.is(typeof wrapper, typeof {});
});
