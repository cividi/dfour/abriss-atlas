import test from 'ava';
import { shallowMount } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import store from '../../src/store';
import i18n from '../../src/trans';
import ContentSection from '../../src/components/landing/ContentSection.vue';

const router = new VueRouter();
const vuetify = new Vuetify();

test('landing/ContentSection.vue', async (t) => {
  const wrapper = shallowMount(ContentSection, {
    vuetify,
    i18n,
    store,
    router,
    propsData: {
      content: '<h3 class="text-h3">Test</h3><p class="text-body-1">Test content</p>'
    }
  });

  t.is(wrapper.findAll('.text-h3').length, 1);
  t.is(wrapper.find('.text-h3').text(), 'Test');
  t.is(wrapper.find('.text-body-1').text(), 'Test content');
});

