import test from 'ava';
import { mount } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import store from '../../src/store';
import i18n from '../../src/trans';
import LandingPage from '../../src/components/LandingPage.vue';

const router = new VueRouter();
const vuetify = new Vuetify();

test('LandingPage.vue', async (t) => {
  const hero = {
    content: '> 84 PROZENT DER ABFÄLLE IN DER SCHWEIZ STAMMEN GEMÄSS BUNDESAMT FÜR UMWELT (BAFU) AUS DER BAUBRANCHE: JEDE SEKUNDE WERDEN SCHWEIZWEIT ÜBER 500 KILOGRAMM BAUABFÄLLE ERZEUGT. <cite>[BAFU](https://www.bafu.admin.ch)</cite>',
    isHero: true,
    image: 'assets/abriss_atlas_map_preview.png'
  };
  const content = [
    {
      content: '### Ausstellung\r\n\r\nWir von [Countdown2030](https://countdown2030.ch) sind überzeugt, es braucht ein Umdenken in unserer Gesellschaft und drastische Veränderungen in der Baubranche, wenn wir die Ziele des Pariser Klimaabkommens erreichen wollen. Denn heute ist die Baubranche für rund einen Drittel der Treibhausgas-Emissionen in der Schweiz verantwortlich.\r\n\r\nIm Bereich Gebäudebetrieb konnten in den letzten Jahren dank wirksamer Gesetze, Einsatz von erneuer- baren Energien sowie Wärmedämmung und -rückgewinnung ökologische Fortschritte erzielt werden. Die Grauen Emissionen [SB2] hingegen, die bei der Erstellung von Gebäuden verursacht werden, sind stetig ge- stiegen. Heute fallen beim Bau eines neuen Gebäudes durchschnittlich so viel Energieverbrauch und Treib- hausgas-Emissionen an, wie während 50 Jahren im Betrieb.\r\n\r\nLösungsansätze für dieses Problem liegen im Erhalt, im Umbau und in der Umnutzung bestehender Gebäu- de. Abrisse und Ersatzneubauten sollten, wenn immer möglich, verhindert werden und nicht länger als ers- te und beste Option gelten. Denn beim Abriss sind Verluste auf verschiedenen Ebenen zu verzeichnen: zum einen gehen die gespeicherte Graue Energie [SB4] und die verwendeten materiellen Ressourcen verloren, und zum anderen auch historische Baukultur, Freiräume und soziale Netzwerke.\r\n\r\nDer Abriss-Atlas soll einerseits die Dimensionen des Abriss in der Schweiz fassbar machen, andererseits die Einzelgeschichten der verschwundenen Häuser erzählen. Der Abriss-Atlas bildet die Grundlage für weitere Aktionen, welche auf dieser Seite ebenfalls vorgestellt werden.',
      isHero: false,
      image: ''
    },
    {
      content: '### Mitmachen\r\n\r\nWir brauchen Deine Hilfe! Möglichst viele Menschen sollen aktiv am Abriss-Atlas mitarbeiten und die Aktionen mitgestalten. Denn wir sind überzeugt: Nur gemeinsam können wir einen Wandel herbeiführen.\r\n\r\n#### Abrissgebäude finden und eintragen\r\nJede und jeder kann ganz einfach über das Formular Gebäude erfassen, Kommentare und Geschichten zu den Häusern schreiben. Gesucht sind Abrissgebäude welche nach 2020 abgerissen wurden oder werden. Zu- verlässige Indizien sind Leerkündigungen, Bauvisiere, Schreiben an Nachbarn und Bautafeln.\r\nWenn du gezielter vorgehen willst: Geplante Abbrüche sind in Bauanzeigen oder Publikationen online in den Amtsblättern der Gemeinden zu finden. Je nach Gemeinde sind die Begriffe etwas anders, bekannte Such- maschinen sind hier eine Hilfe.\r\nEinige Beispiele: [Stadt Bern](), [St. Moritz](), [Köniz]()\r\n\r\n#### Abrissspaziergang\r\nDie Spaziergänge sollen keine traditionellen Führungen sein, sondern einen Dialog fördern, der beim Gehen entsteht. Die Abriss- Spaziergänge befassen sich mit Abriss in den Nachbarschaften der Organisierenden und sollen die Teilnehmenden ermuntern, sich mit dem Thema Abriss auseinanderzusetzen, ihre Meinung zu äussern, sich lokal zu verknüpfen und Netzwerke aufbauen, um gemeinsam aktiv zu werden.\r\nDu möchtest einen Abrissspaziergang organisieren? Dann melde Dich bei: [walk@countdown2030.ch](mailto:walk@countdown2030.ch)\r\n\r\n#### Bauplakat\r\nUm den Abriss kritisch zu hinterfragen, stellt Countdown 2030 Bauplakate in zwei unterschiedlichen Grös- sen und in drei verschiedenen Sprachen an alle Bauenden zur Verfügung, welche unsere Botschaft ans Bau- gerüst hängen wollen:\r\n\r\n> HIER WIRD RENOVIERT STATT DEMOLIERT!\r\n\r\n> ICI ON RÉNOVÉ – AU LIEU DE DÉMOLIR!\r\n\r\n> QUI NON DEMOLIAMO – MA RINNOVIAMO!\r\n\r\n> HIER WIRD UMGEBAUT – STATT ABGERISSEN!\r\n\r\nDu hast eine Baustelle? Dann melde Dich bei: [bauplakat@countdown2030.ch](mailto:countdown2030.ch)',
      isHero: false,
      image: ''
    },
    {
      content: '### Ausstellung\r\nVom 3. September – 23. Oktober 2022 findet im S AM Schweizerischen Architekturmuseum in Basel die Aus- stellung «Die Schweiz: Ein Abriss» statt. Wir von Countdown2030 haben die Ausstellung kuratiert und sind während der ganzen Ausstellungsdauer vor Ort im Museum zu finden. Im Rahmen der Ausstellung gibt es ein Begleitprogramm, alle Infos findest Du auf der Seite des [S AM Kalenders]().\r\n\r\n![Ausstellungsplakat, Claudiabasel][exhibition]\r\n\r\n[exhibition]: https://abrissatlas.eu-central-1.linodeobjects.com/assets/abriss_atlas_plakat_ausstellung.png "Ausstellungsplakat, Claudiabasel"',
      isHero: false,
      image: ''
    },
    {
      content: '### Petition: «Fertig mit dem Schweizer Abriss Wahn!»\r\nIm Rahmen der Ausstellung lancieren wir eine nationale Petition in allen vier Landessprachen und Englisch. Die Petition kann vor Ort in der Ausstellung oder online unterschrieben werden. Wir werden die Petition und alle gesammelten Unterschriften im Anschluss an die Ausstellung in Bundesbern dem Parlament und dem Bundesrat überreichen.\r\nWir fordern:\r\n1. ABRISS ALS AUSNAHME\r\n2. FERTIG MIT FEHLANREIZEN\r\n3. MEHR BAUEN IM BESTAND\r\n4. KLARE ZIELE FÜR ALLE BAUTEN\r\n5. DIE ÖFFENTLICHE HAND GEHT VORAN\r\n\r\nAb dem 2. September kannst du die Petition online lesen und unterschreiben.\r\n\r\nDie Petition kann im Wortlaut übernommen und auch kantonal oder auf Gemeindeebene eingereicht werden. Entsprechende Vorlagen stellen wir gerne zur Verfügung! Melde Dich unter [now@countdown2030.ch](mailto:now@countdown2030.ch)',
      isHero: false,
      image: ''
    },
    {
      content: '### Kontakt\r\nDiese Website ist im Aufbau. Wir arbeiten partizipatorisch und verbessern im Prozess. Bei Fragen, Fehlern oder Rückmeldungen melde Dich bei [atlas@countdown2030.ch](mailto:atlas@countdown2030.ch)',
      isHero: false,
      image: ''
    },
    {
      content: '### Impressum\r\n#### Lancierung\r\n* [Countdown 2030](https://countdown2030.ch), Verein für zukunftsfähige Baukultur\r\nProjektleitung Abriss-Atlas: Leon Faust\r\nKernteam Ausstellung: Rahel Dürmüller, Valerio Dorn, Leon Faust und Oliver Zbinden\r\n* Programmierung: [dfour](https://www.dfour.ch) by [cividi](https://www.cividi.ch)\r\n* Grafik: [Karen Trachsel](https://www.nerak.ch)\r\n\r\n#### Partner\r\n* S AM Schweizerisches Architekturmuseum\r\n* Professur für Konstruktionserbe und Denkmalpflege\r\n* ETH Zürich, Prof. Silke Langenberg\r\n* Schweizer Heimatschutz\r\n\r\n#### Medienpartner\r\n* Hochparterre\r\n* espazium\r\n* Tec21\r\n* Archi\r\n* Tracé\r\n* Baublatt\r\n* Docu Media',
      isHero: false,
      image: ''
    }
  ];
  const wrapper = mount(LandingPage, {
    vuetify,
    i18n,
    store,
    router,
    propsData: {
      fetchedData: [hero, ...content],
      loading: false
    }
  });

  t.is(wrapper.findAll('.hero').length, 1);
  t.is(wrapper.findAll('.section').length, content.length);
});
