const fs = require('fs')

process.env.VUE_APP_GIT_VERSION = 'NaN.NaN.NaN\tNaN';

try {
  process.env.VUE_APP_GIT_VERSION = `${fs.readFileSync('VERSION', 'utf8')}\t${process.env.VERCEL_GIT_COMMIT_SHA}`;
} catch (err) {
  // pass
}


module.exports = {
  devServer: {
    compress: true,
    // inline: true,
    port: '8080',
    host: 'www.local',
    // https: true,
    allowedHosts: [
      'www',
      'www.local',
      'localhost',
      'vue'
    ]
  },
  transpileDependencies: [
    'vuetify'
  ],

  runtimeCompiler: true,

  chainWebpack: (config) => {
    config.module
      .rule('i18n')
      .resourceQuery(/blockType=i18n/)
      .type('javascript/auto')
      .use('i18n')
      .loader('@intlify/vue-i18n-loader');
  }

  // pluginOptions: {
  // prerenderSpa: {
  // registry: undefined,
  // renderRoutes: [
  //   '/de/',
  //   '/fr/',
  //   '/en/',
  //   '/it/'
  // ],
  // useRenderEvent: true,
  // headless: true,
  // onlyProduction: true
  // customRendererConfig: {
  //   executablePath: '/usr/bin/chromium-browser',
  //   args: [
  //     '--no-sandbox',
  //     '--disable-dev-shm-usage'
  //   ]
  // }
  // }
  // }
};
