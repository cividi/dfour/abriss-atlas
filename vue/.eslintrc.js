module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    quotes: [
      'error',
      'single'
    ],
    'comma-dangle': [
      'error',
      'never'
    ],
    'template-curly-spacing': 'off',
    indent: ['error', 2, {
      ignoredNodes: ['TemplateLiteral']
    }],
    'func-names': 'off',
    'no-param-reassign': 'off',
    'no-new': 'off',
    'no-shadow': 'off',
    'prefer-destructuring': 'off',
    'no-prototype-builtins': 'off',
    end_of_line: 'lf',
    'vue/multi-word-component-names': 'off',
    'vue/no-mutating-props': 'off',
    'vue/no-v-text-v-html-on-component': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
