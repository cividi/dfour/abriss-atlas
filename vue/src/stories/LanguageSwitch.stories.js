import LanguageSwitch from '../components/LanguageSwitch.vue';

export default {
  title: 'Abriss Atlas/Shared/Molecules',
  component: LanguageSwitch
};

const Template = (args, { argTypes }) => ({
  components: {
    LanguageSwitch
  },
  props: Object.keys(argTypes),
  template: '<div style="display: flex; justify-content: right;"><LanguageSwitch v-bind="$props" /></div>'
});

export const LanguageSwitcher = Template.bind({});
LanguageSwitcher.args = {
    expanded: 1
}
