import HeroSection from '../../components/landing/HeroSection.vue';

export default {
  title: 'Abriss Atlas/Landing Page/Molecules',
  component: HeroSection
};

const Template = (args, { argTypes }) => ({
  components: {
    HeroSection
  },
  props: Object.keys(argTypes),
  template: '<HeroSection v-bind="$props" />',
});

export const Hero = Template.bind({});
Hero.args = {
  data: {
    content: `<blockquote>
    <p class="text-body-1">84 PROZENT DER ABFÄLLE IN DER SCHWEIZ STAMMEN GEMÄSS BUNDESAMT FÜR UMWELT (BAFU) AUS DER BAUBRANCHE: JEDE SEKUNDE WERDEN SCHWEIZWEIT ÜBER 500 KILOGRAMM BAUABFÄLLE ERZEUGT. <cite><a href="https://www.bafu.admin.ch">BAFU</a></cite></p>
    </blockquote>`,
    image: 'https://abrissatlas.eu-central-1.linodeobjects.com/assets/abriss_atlas_map_preview.png'
  }  
}
Hero.storyName = 'Hero Section';
