import SubMenu from '../../components/landing/SubMenu.vue';

export default {
  title: 'Abriss Atlas/Landing Page/Molecules',
  component: SubMenu
};

export const Menu = () => ({
  components: {
    SubMenu
  },
  template: '<SubMenu />'
});

Menu.storyName = 'Submenu Section';
