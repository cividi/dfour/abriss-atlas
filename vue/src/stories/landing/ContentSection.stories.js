import ContentSection from '../../components/landing/ContentSection.vue';

export default {
  title: 'Abriss Atlas/Landing Page/Molecules/Content Section',
  component: ContentSection,
  argTypes: {
    align: {
      options: ['left', 'right'],
      control: { type: 'inline-radio' }
    }
  }
};

const Template = (args, { argTypes }) => ({
  components: {
    ContentSection
  },
  props: Object.keys(argTypes),
  template: '<ContentSection v-bind="$props" />',
});

export const TextBlock = Template.bind({});
TextBlock.args = { title: 'Ausstellung', align: 'left', content: '<h3 class="text-h3">Ausstellung</h3><p class="text-body-1">Wir von <a href="https://www.countdown2030.ch">Countdown2030</a> sind überzeugt, es braucht ein Umdenken in unserer Gesellschaft und drastische Veränderungen in der Baubranche, wenn wir die Ziele des Pariser Klimaabkommens erreichen wollen. Denn heute ist die Baubranche für rund einen Drittel der Treibhausgas-Emissionen in der Schweiz verantwortlich.</p><p class="text-body-1">Im Bereich Gebäudebetrieb konnten in den letzten Jahren dank wirksamer Gesetze, Einsatz von erneuer- baren Energien sowie Wärmedämmung und -rückgewinnung ökologische Fortschritte erzielt werden. Die Grauen Emissionen [SB2] hingegen, die bei der Erstellung von Gebäuden verursacht werden, sind stetig ge- stiegen. Heute fallen beim Bau eines neuen Gebäudes durchschnittlich so viel Energieverbrauch und Treib- hausgas-Emissionen an, wie während 50 Jahren im Betrieb.</p><p class="text-body-1">Lösungsansätze für dieses Problem liegen im Erhalt, im Umbau und in der Umnutzung bestehender Gebäu- de. Abrisse und Ersatzneubauten sollten, wenn immer möglich, verhindert werden und nicht länger als ers- te und beste Option gelten. Denn beim Abriss sind Verluste auf verschiedenen Ebenen zu verzeichnen: zum einen gehen die gespeicherte Graue Energie [SB4] und die verwendeten materiellen Ressourcen verloren, und zum anderen auch historische Baukultur, Freiräume und soziale Netzwerke.</p><p class="text-body-1">Der Abriss-Atlas soll einerseits die Dimensionen des Abriss in der Schweiz fassbar machen, andererseits die Einzelgeschichten der verschwundenen Häuser erzählen. Der Abriss-Atlas bildet die Grundlage für weitere Aktionen, welche auf dieser Seite ebenfalls vorgestellt werden.</p>' }

export const Quotes = Template.bind({});
Quotes.args = { title: 'Mitmachen!', align: 'right', content: '<h3 class="text-h3">Mitmachen!</h3><h4 class="text-h4">Bauplakat</h4><p class="text-body-1">Um den Abriss kritisch zu hinterfragen, stellt Countdown 2030 Bauplakate in zwei unterschiedlichen Grössen und in drei verschiedenen Sprachen an alle Bauenden zur Verfügung, welche unsere Botschaft ans Baugerüst hängen wollen:</p><blockquote>HIER WIRD RENOVIERT STATT DEMOLIERT!</blockquote><blockquote>ICI ON RÉNOVÉ – AU LIEU DE DÉMOLIR!</blockquote><blockquote>QUI NON DEMOLIAMO – MA RINNOVIAMO!</blockquote><blockquote>HIER WIRD UMGEBAUT – STATT ABGERISSEN!</blockquote><p class="text-body-1">Du hast eine Baustelle? Dann melde Dich bei: <a href="emailto:bauplakat@countdown2030.ch">bauplakat@countdown2030.ch</a></p>' };

export const OrderdList = Template.bind({});
OrderdList.args = { align: 'right', content: '<h3 class="text-h3">Petition: «Fertig mit dem Schweizer Abriss Wahn!»</h3><p class="text-body-1">Im Rahmen der Ausstellung lancieren wir eine nationale Petition in allen vier Landessprachen und Englisch. Die Petition kann vor Ort in der Ausstellung oder online unterschrieben werden. Wir werden die Petition und alle gesammelten Unterschriften im Anschluss an die Ausstellung in Bundesbern dem Parlament und dem Bundesrat überreichen.</p><p class="text-body-1">Wir fordern:<ol><li>ABRISS ALS AUSNAHME</li><li>FERTIG MIT FEHLANREIZEN</li><li>MEHR BAUEN IM BESTAND</li></ol></p><p class="text-body-1">Ab dem 2. September kannst du die Petition online lesen und unterschreiben.</p>' };

export const UnorderdList = Template.bind({});
UnorderdList.args = { align: 'left', content: '<h3 class="text-h3">Impressum</h3><h4 class="text-h4">Lancierung</h4><ul><li><a href="https://countdown2030.ch">Countdown 2030</a>, Verein für zukunftsfähige Baukultur<br>Projektleitung Abriss-Atlas: Leon Faust<br>Kernteam Ausstellung: Rahel Dürmüller, Valerio Dorn, Leon Faust und Oliver Zbinden</li><li>Programmierung: <a href="https://www.dfour.ch">dfour</a> by <a href="https://www.cividi.ch">cividi</a></li><li>Grafik: Karen Trachsel</li></ul>' };
