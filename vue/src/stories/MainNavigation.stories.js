import { within, userEvent } from '@storybook/testing-library';

import MainNavigation from "../components/MainNavigation.vue";

export default {
  title: "Abriss Atlas/Shared/Molecules/MainMenu",
  component: MainNavigation,
};

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const Template = (args, { argTypes }) => ({
  components: {
    MainNavigation,
  },
  props: Object.keys(argTypes),
  template: '<MainNavigation v-bind="$props" />',
});

export const Desktop = Template.bind({});
Desktop.parameters = {
  viewport: {
    defaultViewport: "ipadL",
  },
  chromatic: { viewports: [1024, 1280, 1980, 2500] },
};
Desktop.args = {};

export const Mobile = Template.bind({});
Mobile.parameters = {
  viewport: {
    defaultViewport: "iphonese2",
  },
  chromatic: { viewports: [320, 375, 428, 926] },
};
Mobile.args = {};
