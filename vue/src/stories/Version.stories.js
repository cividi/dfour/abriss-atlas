import Version from '../components/Version.vue';

export default {
  title: 'Abriss Atlas/Shared/Molecules',
  component: Version
};

export const VersionNumber = () => ({
  components: {
    Version
  },
  template: '<v-main><Version /></v-main>'
});
