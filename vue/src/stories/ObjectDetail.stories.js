import { VMain } from 'vuetify/lib';
import ObjectDetail from '../components/ObjectDetail.vue';
import MainNavigation from '../components/MainNavigation.vue';
import LanguageSwitch from '../components/LanguageSwitch.vue';

const fetchedData = [
  {
    enableLikes: false,
    object: {
      id: 'QW5ub3RhdGlvbk5vZGU6MQ==',
      pk: 1,
      kind: 'OBJ',
      rating: 0,
      data: {
        type: 'Feature',
        geometry: { type: 'Point', coordinates: [8.327636718748892, 46.907318236730674] },
        properties: {
          fill: true,
          other: '',
          title: 'Ächerlistrasse',
          moreinfo: 'Die Pavillons waren nur für 30 Jahre geplant und müssen jetzt einem neuen Physikgebäude weichen.',
          subtitle: '6372, Kerns',
          description: 'Das Postbetriebsgebäude in Basel besteht aus zwei miteinander verbundenen Baukörpern: Dem über dem Gleisfeld liegenden, etwas niedrigeren Reiterbau, auch «Platte» genannt, sowie dem «Festlandbau» im Strassengeviert von Post-Passage, Peter Merian-Strasse, Nauenstrasse und Gartenstrasse gelegen. Die Zweiteilung des Gebäudes tritt an der Fassade des insgesamt 101 x 186 m messenden, relativ flachen Baus nicht in Erscheinung.',
          commentAuthor: 'Max Mustermann',
          demolitionYear: '2024',
          constructionYear: '1966',
        },
      },
      category: { pk: 1, name: 'Wohnen', icon: '', color: '#cccccc', commentsEnabled: true },
      state: { pk: 1, name: 'bedroht', decoration: 'GRAY' },
      attachements: [
        { document: 'assets/sample_1.jpg', myOrder: 0 },
        { document: 'assets/sample_2.jpeg', myOrder: 1 },
      ],
    },
  },
  {
    enableLikes: false,
    object: {
      id: 'QW5ub3RhdGlvbk5vZGU6MQ==',
      pk: 1,
      kind: 'OBJ',
      rating: 0,
      data: {
        type: 'Feature',
        geometry: { type: 'Point', coordinates: [8.327636718748892, 46.907318236730674] },
        properties: {
          fill: true,
          other: '',
          title: 'Ächerlistrasse',
          subtitle: '6372, Kerns',
          description: 'Das Postbetriebsgebäude in Basel besteht aus zwei miteinander verbundenen Baukörpern: Dem über dem Gleisfeld liegenden, etwas niedrigeren Reiterbau, auch «Platte» genannt, sowie dem «Festlandbau» im Strassengeviert von Post-Passage, Peter Merian-Strasse, Nauenstrasse und Gartenstrasse gelegen. Die Zweiteilung des Gebäudes tritt an der Fassade des insgesamt 101 x 186 m messenden, relativ flachen Baus nicht in Erscheinung.',
          commentAuthor: 'Max Mustermann',
          demolitionYear: '2024',
          constructionYear: '1966',
        },
      },
      category: { pk: 1, name: 'Wohnen', icon: '', color: '#cccccc', commentsEnabled: true },
      state: { pk: 1, name: 'bedroht', decoration: 'GRAY' },
      attachements: [
        { document: 'assets/sample_1.jpg', myOrder: 0 }
      ],
    },
  },
  {
    enableLikes: false,
    object: {
      id: 'QW5ub3RhdGlvbk5vZGU6MQ==',
      pk: 1,
      kind: 'OBJ',
      rating: 0,
      data: {
        type: 'Feature',
        geometry: { type: 'Point', coordinates: [8.327636718748892, 46.907318236730674] },
        properties: {
          fill: true,
          other: '',
          title: 'Ächerlistrasse',
          subtitle: '6372, Kerns',
          commentAuthor: 'Max Mustermann',
          demolitionYear: '2024',
          constructionYear: '1966',
        },
      },
      category: { pk: 1, name: 'Wohnen', icon: '', color: '#cccccc', commentsEnabled: true },
      state: { pk: 1, name: 'bedroht', decoration: 'GRAY' },
      attachements: [],
    },
  },
];

export default {
  title: 'Abriss Atlas/Steckbrief',
  component: ObjectDetail
};

const Template = (args, { argTypes }) => ({
  components: {
    ObjectDetail,
    MainNavigation,
    LanguageSwitch,
    VMain
  },
  props: Object.keys(argTypes),
  template: '<div style="margin: -18px;"><v-main><ObjectDetail v-bind="$props" /></v-main></div>',
});

export const Overlay = Template.bind({});
Overlay.parameters = {};
Overlay.args = { ...fetchedData[0] };

export const OneImage = Template.bind({});
OneImage.parameters = {};
OneImage.args = { ...fetchedData[1] };

export const Basic = Template.bind({});
Basic.parameters = {};
Basic.args = { ...fetchedData[2] };
